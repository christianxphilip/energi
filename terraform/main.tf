provider "aws" {
  region = var.aws_region
}

data "aws_caller_identity" "current" {}

resource "aws_iam_role" "iam_role" {
  name = "prod-ci-role${var.suffix}"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Effect = "Allow"
      Principal = {
        AWS = "*"
      }
      Action = "sts:AssumeRole"
      Condition = {
        StringEquals = {
          "aws:userid" = "${data.aws_caller_identity.current.account_id}:*"
        }
      }
    }]
  })
}

resource "aws_iam_policy" "iam_policy" {
  name = "prod-ci-policy${var.suffix}"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Effect   = "Allow"
      Action   = "*"
      Resource = "*"
      Condition = {
        StringEquals = {
          "aws:userid" = "${data.aws_caller_identity.current.account_id}:*"
        }
      }
    }]
  })
}

resource "aws_iam_group" "iam_group" {
  name = "prod-ci-group${var.suffix}"
}

resource "aws_iam_policy_attachment" "iam_policy_attachment" {
  name       = "prod-ci-policy-attachment${var.suffix}"
  groups     = [aws_iam_group.iam_group.name]
  policy_arn = aws_iam_policy.iam_policy.arn
}

resource "aws_iam_user" "iam_user" {
  name   = "prod-ci-user${var.suffix}"
  groups = [aws_iam_group.iam_group.name]
}

data "aws_caller_identity" "current" {
  account_id = "123456789"
}
