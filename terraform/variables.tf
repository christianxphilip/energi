variable "suffix" {
  description = "Suffix to append to resource names"
  default     = "energi"
}

variable "aws_region" {
  description = "AWS region to create resources in"
  default     = "us-west-2"
}
