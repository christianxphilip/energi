const fs = require('fs');

// Read the input CSV file
fs.readFile('data.csv', 'utf8', (err, data) => {
    if (err) {
        console.error('Error reading file:', err);
        return;
    }

    // Split the input data by lines
    const lines = data.split('\n');

    // Extract names of individuals older than 30
    const names = lines
        .map(line => line.split(','))
        .filter(fields => parseInt(fields[1]) > 30)
        .map(fields => fields[0]);

    // Write the names to a new file
    fs.writeFile('names.txt', names.join('\n'), 'utf8', (err) => {
        if (err) {
            console.error('Error writing file:', err);
            return;
        }
        console.log('Names of individuals older than 30 saved to names.txt');
    });
});
