# CSV Data Extraction

This repository contains a shell script (`extract_names.sh`) and a Node.js script (`extract_names.js`) to extract names of individuals older than 30 from a CSV file and save them to a new file.

## Shell Script (`extract_names.sh`)

This shell script uses `awk` and `sed` to extract names of individuals older than 30 from a CSV file. See the script for more details.

### Usage

1. **Add Data**: Ensure you have a CSV file named `data.csv` with the format `name,age,gender`. Add your data to this file.

2. **Run Script**: Run the shell script to extract names older than 30:

    ```sh
    ./extract_names.sh
    ```

3. **Check Output**: Check the `names.txt` file for the extracted names.

## Node.js Script (`extract_names.js`)

This Node.js script reads the contents of the `data.csv` file, extracts names of individuals older than 30, and saves them to a new file.

### Usage

1. **Add Data**: Ensure you have a CSV file named `data.csv` with the format `name,age,gender`. Add your data to this file.

2. **Run Script**: Run the Node.js script to extract names older than 30:

    ```sh
    node extract_names.js
    ```

3. **Check Output**: Check the `names.txt` file for the extracted names.

