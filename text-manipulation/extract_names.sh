#!/bin/sh

# Read the input CSV file and filter out names of individuals older than 30
awk -F',' '$2 > 30 {print $1}' data.csv > temp.txt

# Remove trailing spaces and empty lines
sed '/^[[:space:]]*$/d' temp.txt > names.txt

# Cleanup temp file
rm temp.txt

echo "Names of individuals older than 30 saved to names.txt"
