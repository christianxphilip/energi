# DevOps Challenges

This repository contains solutions for the following DevOps challenges:

## Challenges

1. **Docker Whale:** Write a Dockerfile to run Energi Node in a container. It should somehow verify the checksum of the downloaded release (there's no need to build the project), run as a normal user, and print its output to the console. The build should be security conscious and ideally pass a container image security test such as ECR or Trivy. 
   - **Solution:** Located in the `docker-whale` folder.
   - [Energi Node Download](https://wiki.energi.world/en/downloads/core-node)

2. **K8S Awesomeness:** Write a Kubernetes StatefulSet to run the above, using persistent volume claims and resource limits. 
   - **Solution:** Located in the `k8s` folder.

3. **All the Continuouses:** Write a simple build and deployment pipeline for the above using Groovy/Jenkinsfile, Travis CI, or GitLab CI. 
   - **Solution:** Located in the `.gitlab-ci.yaml` file.

4. **Script Kiddies:** Source or come up with a text manipulation problem and solve it with at least two of awk, sed, tr, and/or grep. 
   - **Solution:** Located in the `text-manipulation` folder.

5. **Script Grown-ups:** Solve the problem in question 4 using any programming language of your choice.
   - **Solution:** Located in the `text-manipulation` folder.

6. **Terraform Lovers:** Write a Terraform module that creates the following resources in IAM:
        - A role with no permissions, which can be assumed by users within the same account.
        - A policy allowing users/entities to assume the above role.
        - A group with the above policy attached.
        - A user belonging to the above group.
        - All four entities should have the same name or be similarly named in some meaningful way given the context (e.g., prod-ci-role, prod-ci-policy, prod-ci-group, prod-ci-user; or just prod-ci). Make the suffixes toggleable, if you wish.
   - **Solution:** Located in the `terraform` folder.

   
## Note

My solutions came up with the help of:
- ChatGPT
- [Energi Installation Guide](https://wiki.energi.world/docs/guides/core-node-linux)
- [GitLab CI/CD Documentation](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [GitHub](https://github.com/cloudposse/terraform-aws-iam-role/tree/main)